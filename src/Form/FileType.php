<?php
/**
 * Created by PhpStorm.
 * User: pza
 * Date: 2018-12-09
 * Time: 22:01
 */

namespace App\Form;


use App\Entity\File;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FileType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('name')
      ->setAction('/file/list')
    ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults([
      'data_class' => File::class,
    ]);
  }
}