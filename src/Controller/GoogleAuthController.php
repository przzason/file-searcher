<?php
/**
 * Created by PhpStorm.
 * User: pza
 * Date: 2018-12-09
 * Time: 23:55
 */

namespace App\Controller;


use App\Entity\GoogleAuth;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GoogleAuthController extends AbstractController
{
  /**
   * @Route("/google-auth/generate")
   * @return Response
   */
  public function generate() {
    $authUrl = GoogleAuth::generateAuthUrl();

    return $this->redirect($authUrl);
  }

  /**
   * @Route("/google-auth/persist")
   * @param Request $request
   * @return Response
   * @throws \Exception
   */
  public function persist(Request $request) {
    $authCode = $request->query->get('code');
    GoogleAuth::setAuth($authCode);

    $response = $this->redirect('/file/search');

    return $response;
  }
}