<?php
/**
 * Created by PhpStorm.
 * User: pza
 * Date: 2018-12-09
 * Time: 21:59
 */

namespace App\Controller;


use App\Entity\File;
use App\Entity\FileFilter;
use App\Entity\FileFilterEmpty;
use App\Form\FileType;
use App\Utils\FileStorageCollectionFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FileController extends AbstractController
{
  /**
   * @Route("/file/list", name="app_file_list")
   * @param Request $request
   * @return Response
   * @throws \Exception
   */
  public function list(Request $request)
  {
    $filename = $request->request->get('file')['name'];
    $filename = empty($filename) ? '' : $filename;

    $files = array();
    foreach (FileStorageCollectionFactory::getAllAuthorized() as $fileStorage) {
      $files = array_merge($files,$fileStorage->getFilesByName($filename));
    }

    $form = $this->createForm(FileType::class, new FileFilter($filename));

    return $this->render('file/list.html.twig', [
      'form' => $form->createView(),
      'files' => $files,
    ]);
  }

  /**
   * @Route("/")
   * @Route("/file/search")
   * @return Response
   */
  public function search() {
    $form = $this->createForm(FileType::class, new FileFilterEmpty());

    return $this->render('base.html.twig', [
      'form' => $form->createView(),
    ]);
  }

}