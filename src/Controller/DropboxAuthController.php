<?php
/**
 * Created by PhpStorm.
 * User: pza
 * Date: 2018-12-10
 * Time: 14:02
 */

namespace App\Controller;


use App\Entity\DropboxAuth;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DropboxAuthController extends AbstractController
{
  /**
   * @Route("/dropbox-auth/generate")
   * @return Response
   * @throws \Exception
   */
  public function generate() {
    $authUrl = DropboxAuth::generateAuthUrl();

    return $this->redirect($authUrl);
  }

  /**
   * @Route("/dropbox-auth/persist")
   * @param Request $request
   * @return Response
   * @throws \Exception
   */
  public function persist(Request $request) {
    $authCode = $request->query->get('code');
    $state = $request->query->get('state');
    DropboxAuth::setAuth($authCode, $state);

    $response = $this->redirect('/file/search');

    return $response;
  }

}