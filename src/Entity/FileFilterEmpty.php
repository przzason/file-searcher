<?php
/**
 * Created by PhpStorm.
 * User: pza
 * Date: 2018-12-13
 * Time: 16:38
 */

namespace App\Entity;


class FileFilterEmpty extends FileFilter
{
  public function __construct() {
    parent::__construct('');
  }
}