<?php
/**
 * Created by PhpStorm.
 * User: pza
 * Date: 2018-12-10
 * Time: 12:43
 */

namespace App\Entity;


use Exception;
use Google_Client;
use Google_Service_Drive;
use Symfony\Component\HttpFoundation\Session\Session;

abstract class GoogleAuth
{
  const CALLBACK_URL = 'http://localhost:8080/google-auth/persist';

  /**
   * @return Google_Client
   * @throws Exception
   */
  public static function getClient()
  {
    $client = self::buildClient();
    $token = self::getToken();
    if (!empty($token)) {
      $client->setAccessToken($token);
    }

    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
      // Refresh the token if possible, else throw error.
      if ($client->getRefreshToken()) {
        $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        self::saveToken($client->getAccessToken());
      } else {
        throw new Exception("Google access token is missing or is unable to be refreshed");
      }
    }
    return $client;
  }

  public static function generateAuthUrl() {
    $client = self::buildClient();
    return $client->createAuthUrl();
  }

  /**
   * @param $authCode
   * @throws Exception
   */
  public static function setAuth($authCode) {
    $client = self::buildClient();
    $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

    // Check to see if there was an error.
    if (array_key_exists('error', $accessToken)) {
      throw new Exception(join(', ', $accessToken));
    }

    $client->setAccessToken($accessToken);
    self::saveToken($client->getAccessToken());
  }

  protected static function buildClient() {
    $client = new Google_Client();
    $client->setApplicationName('Google Drive API PHP');
    $client->setScopes(Google_Service_Drive::DRIVE_METADATA_READONLY);
    $client->setAuthConfig(__DIR__ . '/Config/client_id.json');
    $client->setAccessType('offline');
    $client->setRedirectUri(self::CALLBACK_URL);
    return $client;
  }

  /**
   * @return bool
   */
  public static function isAuthenticated() {
    return empty(self::getToken()) ? false : true;
  }

  protected static function getToken() {
    $session = new Session();
    return $session->get('google-auth-token');
  }

  protected static function saveToken($accessToken) {
    $session = new Session();
    $session->set('google-auth-token', $accessToken);
  }
}