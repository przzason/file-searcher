<?php
/**
 * Created by PhpStorm.
 * User: pza
 * Date: 2018-12-09
 * Time: 22:02
 */

namespace App\Entity;


class File
{
  protected $name;
  protected $location;
  protected $storage;

  /**
   * @return string|null
   */
  public function getName(): string {
    return $this->name;
  }
  
  /**
   * @return string|null
   */
  public function getLocation(): string {
    return $this->location;
  }

  /**
   * @return string|null
   */
  public function getStorage(): string {
    return $this->storage;
  }

  public function __construct(string $name, string $location, string $storage) {
    $this->name = $name;
    $this->location = $location;
    $this->storage = $storage;
  }
}