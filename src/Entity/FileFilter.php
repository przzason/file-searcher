<?php
/**
 * Created by PhpStorm.
 * User: pza
 * Date: 2018-12-13
 * Time: 16:36
 */

namespace App\Entity;


class FileFilter extends File
{
  public function __construct(string $name) {
    parent::__construct($name, '', '');
  }
}