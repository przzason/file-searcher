<?php
/**
 * Created by PhpStorm.
 * User: pza
 * Date: 2018-12-10
 * Time: 13:17
 */

namespace App\Entity;


use Exception;
use Kunnu\Dropbox\Dropbox;
use Kunnu\Dropbox\DropboxApp;
use Symfony\Component\HttpFoundation\Session\Session;

abstract class DropboxAuth
{
  const CALLBACK_URL = 'http://localhost:8080/dropbox-auth/persist';

  /**
   * @return DropboxApp
   * @throws Exception
   * @throws \Exception
   */
  public static function getClient()
  {
    $credentials = self::getClientCredentials();
    $token = self::getToken();
    if(empty($token)) {
      throw new Exception("Dropbox access token is missing!");
    }
    $client = new DropboxApp($credentials['clientId'], $credentials['clientSecret'], $token);

    return $client;
  }

  /**
   * @return string
   * @throws Exception
   */
  public static function generateAuthUrl() {
    $authHelper = self::buildAuthHelper();

    $authUrl = $authHelper->getAuthUrl(self::CALLBACK_URL);

    return $authUrl;

  }

  /**
   * @param $authCode
   * @param $state
   * @throws Exception
   */
  public static function setAuth($authCode, $state) {
    $authHelper = self::buildAuthHelper();

    //Fetch the AccessToken
    $accessToken = $authHelper->getAccessToken($authCode, $state, self::CALLBACK_URL);

    self::saveToken($accessToken->getToken());
  }

  /**
   * @return bool
   */
  public static function isAuthenticated() {
    return empty(self::getToken()) ? false : true;
  }

  /**
   * @return \Kunnu\Dropbox\Authentication\DropboxAuthHelper
   * @throws Exception
   */
  protected static function buildAuthHelper() {
    $credentials = self::getClientCredentials();
    $client = new DropboxApp($credentials['clientId'], $credentials['clientSecret']);
    $dropbox = new Dropbox($client);
    $authHelper = $dropbox->getAuthHelper();

    return $authHelper;
  }

  /**
   * @return array
   * @throws Exception
   */
  protected static function getClientCredentials() {
    $tokenPath = __DIR__ . '/Config/dropbox-access.json';
    if (file_exists($tokenPath)) {
      $json_file = json_decode(file_get_contents($tokenPath), true);

      return array(
        'clientId' => $json_file['clientId'],
        'clientSecret' => $json_file['clientSecret'],
      );
    } else {
      throw new Exception("Dropbox access file is missing");
    }
  }

  protected static function getToken() {
    $session = new Session();
    return $session->get('dropbox-auth-token');
  }

  protected static function saveToken($accessToken) {
    $session = new Session();
    $session->set('dropbox-auth-token', $accessToken);
  }

}