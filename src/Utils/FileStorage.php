<?php
/**
 * Created by PhpStorm.
 * User: pza
 * Date: 2018-12-13
 * Time: 16:53
 */

namespace App\Utils;

abstract class FileStorage implements FileStorageInterface
{
  final public function getFilesByName(string $filename) {
    if(strlen($filename) == 0) {
      throw new \Exception("No search input");
    }

    return $this->searchByName($filename);
  }

  protected abstract function searchByName(string $filename);
}