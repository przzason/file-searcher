<?php
/**
 * Created by PhpStorm.
 * User: pza
 * Date: 2018-12-09
 * Time: 22:47
 */

namespace App\Utils;


use App\Entity\DropboxAuth;
use App\Entity\GoogleAuth;

abstract class FileStorageCollectionFactory
{
  /**
   * @return FileStorageInterface[]
   * @throws \Exception
   */
  public static function getAllAuthorized() : array {
    if(DropboxAuth::isAuthenticated()) {
      $fileStorage[] = new DropboxStorage(DropboxAuth::getClient());
    }

    if(GoogleAuth::isAuthenticated()) {
      $fileStorage[] = new GoogleDriveStorage(GoogleAuth::getClient());
    }

    $fileStorage[] = new LocalDriveStorage();

    return $fileStorage;
  }

}