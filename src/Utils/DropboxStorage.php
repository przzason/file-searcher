<?php
/**
 * Created by PhpStorm.
 * User: pza
 * Date: 2018-12-09
 * Time: 22:36
 */

namespace App\Utils;


use App\Entity\File;
use Kunnu\Dropbox\Dropbox;
use Kunnu\Dropbox\DropboxApp;

class DropboxStorage extends FileStorage
{
  const NAME = 'Dropbox';
  protected $service;

  /**
   * DropboxStorage constructor.
   * @param DropboxApp $app
   */
  public function __construct(DropboxApp $app) {
    $this->service = new Dropbox($app);
  }

  /**
   * @param string $filename
   * @return File[]
   */
  protected function searchByName(string $filename) {
    return $this->getFiles('/', $filename);
  }

  /**
   * @param string $path file directory path to search
   * @param string $query search query
   * @param array $params additional parameters
   * @link https://github.com/kunalvarma05/dropbox-php-sdk/wiki/Working-with-files#search see for more details on all above parameters
   * @return File[]
   */
  protected function getFiles(string $path, string $query, array $params = array()) {
    $results = $this->service->search($path, $query, $params);

    $files = array();
    foreach($results->getItems()->all() as $file) {
      $files[] = new File($file->getMetadata()->getName(),$file->getMetadata()->getPathDisplay(), self::NAME);
    }

    return $files;
  }
}