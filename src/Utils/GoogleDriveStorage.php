<?php
/**
 * Created by PhpStorm.
 * User: pza
 * Date: 2018-12-09
 * Time: 23:21
 */

namespace App\Utils;


use App\Entity\File;
use Exception;
use Google_Client;
use Google_Service_Drive;

class GoogleDriveStorage extends FileStorage
{
  const NAME = 'Google drive';
  protected $service;

  /**
   * GoogleDriveStorage constructor.
   * @param Google_Client $client
   */
  public function __construct(Google_Client $client) {
    $this->service = new Google_Service_Drive($client);
  }

  /**
   * @param string $filename
   * @return File[]
   * @throws Exception
   */
  protected function searchByName(string $filename) {
    return $this->getFiles($optParams = array(
      'q' => "name contains '{$filename}'",
      'fields' => 'files(name, webViewLink)'
    ));
  }

  /**
   * @param $optParams
   * @return File[]
   */
  protected function getFiles($optParams) : array {
    $results = $this->service->files->listFiles($optParams);

    $files = array();
    foreach ($results->getFiles() as $file) {
      $files[] = new File($file->getName(), $file->getWebViewLink(),self::NAME);
    }

    return $files;
  }
}