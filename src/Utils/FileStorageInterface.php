<?php
/**
 * Created by PhpStorm.
 * User: pza
 * Date: 2018-12-09
 * Time: 22:21
 */

namespace App\Utils;


use App\Entity\File;

interface FileStorageInterface
{
  /**
   * @param string $filename
   * @return File[]
   * @throws \Exception
   */
  public function getFilesByName(string $filename);
}