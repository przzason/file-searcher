<?php
/**
 * Created by PhpStorm.
 * User: pza
 * Date: 2018-12-09
 * Time: 22:24
 */

namespace App\Utils;


use App\Entity\File;

class LocalDriveStorage extends FileStorage
{
  const NAME = 'Local drive';

  /**
   * @param string $filename
   * @return File[]
   */
  protected function searchByName(string $filename) {
    $config = json_decode(file_get_contents(__DIR__.'/Config/local-storage-config.json'), true);
    $path = $config['root'];

    $directory = new \RecursiveDirectoryIterator($path);
    $iterator = new \RecursiveIteratorIterator($directory);
    $files = array();

    foreach ($iterator as $info) {
      if (!$info->isDir() && strpos($info->getFilename(), $filename) !== false) {
        $files[] = new File($info->getBasename(), $info->getPath(), self::NAME);
      }
    }

    return $files;
  }

}