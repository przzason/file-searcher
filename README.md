# File Searcher

### Summary

This project is a multi-storage file searcher web application built using symfony 4 framework.
I'm using docker for quick set-up and clean separation of environment on which it operates. 

Current version of this project supports local drive searching (on this web application's server), 
as well as google drive search (via Google Drive APIs web application) and 
drop box search (via dropbox-php-sdk, found here: https://github.com/kunalvarma05/dropbox-php-sdk).
Authorization to google drive/dropbox is done via appropriate buttons in the applications that 
generate authentication URLs and callbacks. 

Search functionality is limited to what the Dropbox API/Google API is limited to 
(searching for specified string as a prefix of filename), 
for consistency the same searching rules apply for local drive searching.
Also, currently you're limited to authorize on each of the services once, meaning only 
one google account and/or one dropbox account can be searched at once - for a total of 3 
searched storages.

As for expanding functionality, this project is ready for additional storage providers, 
each of those needing a PHP class to handle searching/client building, but at the same time
inheriting from the same base interface for the purpose of authorized storages collection.

Additionally the authorization endpoint should be refactored to a generic url-authorization with callback 
endpoint instead of each endpoint for each provider. After that, all storage service providers 
using url-authorization to authorize would be handled by the same endpoint. 
This does not rule out the possibility of storage service provider using authorization that is different 
to url-authorization with callback, such cases would require separate authentication handling.   

### Installation guide

Go to the project's directory and:


1. Go into docker directory ```cd docker```

2. ```docker-compose build```

3. ```docker-compose up -d```

4. Go inside docker container ```docker exec -it docker_php_1 bash```

5. ```composer install```

### Configuration 

1. Local drive root directory config file location ```src/Utils/Config/local-storage-config.json```
2. Dropbox/Google authentication config files located in ```src/Entity/Config/```


### Known bugs

1. Dropbox returns no results when no filename is specified, regardless of any files existing 
on authorized Dropbox account
